System.register(["angular2/core", "./login/service/user.service", "./shared/router/home.router", "angular2/router", "./shared/component/home.component", "./shared/component/navbar.component"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, user_service_1, home_router_1, router_1, home_component_1, navbar_component_1;
    var Main;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (user_service_1_1) {
                user_service_1 = user_service_1_1;
            },
            function (home_router_1_1) {
                home_router_1 = home_router_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (home_component_1_1) {
                home_component_1 = home_component_1_1;
            },
            function (navbar_component_1_1) {
                navbar_component_1 = navbar_component_1_1;
            }],
        execute: function() {
            Main = (function () {
                function Main(_userService, _router) {
                    this._userService = _userService;
                    this._router = _router;
                    if (!this.isLoggedIn) {
                        this._router.navigate(["Default"]);
                    }
                    else {
                        this._router.navigate(["/Navigation/Chatter"]);
                    }
                }
                Object.defineProperty(Main.prototype, "isLoggedIn", {
                    get: function () {
                        return this._userService.isLoggedIn;
                        ;
                    },
                    enumerable: true,
                    configurable: true
                });
                Main = __decorate([
                    router_1.RouteConfig([
                        {
                            path: "/...",
                            name: "Navigation",
                            component: home_router_1.HomeRouter
                        },
                        {
                            path: "/",
                            name: "Default",
                            component: home_component_1.HomeComponent,
                            useAsDefault: true
                        }
                    ]),
                    core_1.Component({
                        selector: "main",
                        template: "<div class=\"container-fluid\">\n                        <navbar [is-logged-in]=\"isLoggedIn\"></navbar>\n                        <div style=\"padding-bottom: 10%;\"></div>\n                        <homerouter></homerouter>\n                  </div>",
                        directives: [home_router_1.HomeRouter, navbar_component_1.NavbarComponent]
                    }), 
                    __metadata('design:paramtypes', [user_service_1.UserService, router_1.Router])
                ], Main);
                return Main;
            }());
            exports_1("Main", Main);
        }
    }
});
