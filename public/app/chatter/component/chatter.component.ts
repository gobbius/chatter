import {Component} from "angular2/core";

@Component({
    selector: "chatter",
    templateUrl: "app/chatter/template/chatter.template.html",
    styles: [`
                .panel-body{
                    height: 40vh;
                }
                textarea {
                    resize: none;
                    font-size: 11px;
                    background: #fff;
                }
                .panel-footer>.row>div:first-child{
                    padding-bottom: 1%;
                }
            `]
})

export class ChatterComponent {
    
    constructor() {
        
    }
    
    
}