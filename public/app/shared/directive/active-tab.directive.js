System.register(["angular2/core"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var ActiveTabDirective;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            ActiveTabDirective = (function () {
                function ActiveTabDirective(_el, _renderer) {
                    this._el = _el;
                    this._renderer = _renderer;
                }
                ActiveTabDirective.prototype.onClick = function ($event) {
                    var _this = this;
                    var parent = $(this._el.nativeElement).parent();
                    var tabs = $(parent).children();
                    tabs.each(function (index, obj) {
                        var link = $(obj).find("a");
                        _this._renderer.setElementClass(link.get(link.length - 1), _this.className, false);
                    });
                    this._renderer.setElementClass($event.target, this.className, true);
                };
                __decorate([
                    core_1.Input("class-name"), 
                    __metadata('design:type', String)
                ], ActiveTabDirective.prototype, "className", void 0);
                ActiveTabDirective = __decorate([
                    core_1.Directive({
                        selector: "[is-active]",
                        host: {
                            "(click)": "onClick($event)",
                        }
                    }), 
                    __metadata('design:paramtypes', [core_1.ElementRef, core_1.Renderer])
                ], ActiveTabDirective);
                return ActiveTabDirective;
            }());
            exports_1("ActiveTabDirective", ActiveTabDirective);
        }
    }
});
