import {Directive, ElementRef, Renderer, Input} from "angular2/core";

@Directive({
    selector: "[is-active]",
    host: {
        "(click)": "onClick($event)",
    }
})

export class ActiveTabDirective {

    @Input("class-name") className:string;

    constructor(private _el:ElementRef, private _renderer:Renderer) {

    }

    onClick($event) {
        
        let parent = $(this._el.nativeElement).parent();
        let tabs = $(parent).children();
        tabs.each((index, obj) => {
            let link = $(obj).find("a");
            this._renderer.setElementClass(link.get(link.length - 1), this.className, false);
        });
        this._renderer.setElementClass($event.target, this.className, true);
    }

}