System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var PasswordValidator;
    return {
        setters:[],
        execute: function() {
            PasswordValidator = (function () {
                function PasswordValidator() {
                }
                PasswordValidator.confirmPassword = function (controlGroup) {
                    if (_.isEqual(controlGroup.find("password").value, controlGroup.find("confirm").value)) {
                        return null;
                    }
                    else {
                        return { confirmPassword: true };
                    }
                };
                return PasswordValidator;
            }());
            exports_1("PasswordValidator", PasswordValidator);
        }
    }
});
