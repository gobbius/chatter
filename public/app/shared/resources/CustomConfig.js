System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var CustomConfig;
    return {
        setters:[],
        execute: function() {
            CustomConfig = (function () {
                function CustomConfig() {
                }
                Object.defineProperty(CustomConfig, "SERVICE_ENDPOINT", {
                    get: function () {
                        return "http://localhost:3000/api";
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(CustomConfig, "API_PREFIX_MAP", {
                    get: function () {
                        return {
                            login: "/login",
                            authorization: "/authorization",
                            signup: "/signup"
                        };
                    },
                    enumerable: true,
                    configurable: true
                });
                return CustomConfig;
            }());
            exports_1("CustomConfig", CustomConfig);
        }
    }
});
