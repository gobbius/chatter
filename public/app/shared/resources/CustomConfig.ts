export class CustomConfig {
    public static get SERVICE_ENDPOINT():string {
        return "http://localhost:3000/api";
    }

    public static get API_PREFIX_MAP():{[key:string]:string;} {
        return {
            login: "/login",
            authorization: "/authorization",
            signup: "/signup"
        };
    }

}