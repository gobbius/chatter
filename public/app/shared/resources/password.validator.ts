import {ControlGroup} from "angular2/common";
export class PasswordValidator {

    static confirmPassword(controlGroup:ControlGroup):{[key:string]:boolean} {
        if (_.isEqual(controlGroup.find("password").value, controlGroup.find("confirm").value)) {
            return null;
        } else {
            return {confirmPassword: true};
        }
    }
}