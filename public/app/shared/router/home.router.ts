import {RouteConfig, ROUTER_DIRECTIVES, Router} from "angular2/router";
import {Component} from "angular2/core";
import {HomeComponent} from "../component/home.component";
import {SigninComponent} from "../../login/component/signin.component";
import {SignUpComponent} from "../../login/component/signup.component";
import {ChatterComponent} from "../../chatter/component/chatter.component";

@RouteConfig([
    {
        path: "/signin",
        name: "Signin",
        component: SigninComponent
    },
    {
        path: "/signup",
        name: "Signup",
        component: SignUpComponent
    },
    {
        path: "/chatter",
        name: "Chatter",
        component: ChatterComponent
    }
])

@Component({
    selector: "homerouter",
    template: "<router-outlet></router-outlet>",
    directives: [ROUTER_DIRECTIVES]
})

export class HomeRouter {

    constructor(private _router:Router){
        
    }
}