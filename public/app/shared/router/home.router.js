System.register(["angular2/router", "angular2/core", "../../login/component/signin.component", "../../login/component/signup.component", "../../chatter/component/chatter.component"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var router_1, core_1, signin_component_1, signup_component_1, chatter_component_1;
    var HomeRouter;
    return {
        setters:[
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (signin_component_1_1) {
                signin_component_1 = signin_component_1_1;
            },
            function (signup_component_1_1) {
                signup_component_1 = signup_component_1_1;
            },
            function (chatter_component_1_1) {
                chatter_component_1 = chatter_component_1_1;
            }],
        execute: function() {
            HomeRouter = (function () {
                function HomeRouter(_router) {
                    this._router = _router;
                }
                HomeRouter = __decorate([
                    router_1.RouteConfig([
                        {
                            path: "/signin",
                            name: "Signin",
                            component: signin_component_1.SigninComponent
                        },
                        {
                            path: "/signup",
                            name: "Signup",
                            component: signup_component_1.SignUpComponent
                        },
                        {
                            path: "/chatter",
                            name: "Chatter",
                            component: chatter_component_1.ChatterComponent
                        }
                    ]),
                    core_1.Component({
                        selector: "homerouter",
                        template: "<router-outlet></router-outlet>",
                        directives: [router_1.ROUTER_DIRECTIVES]
                    }), 
                    __metadata('design:paramtypes', [router_1.Router])
                ], HomeRouter);
                return HomeRouter;
            }());
            exports_1("HomeRouter", HomeRouter);
        }
    }
});
