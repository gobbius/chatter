System.register(["angular2/core"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var SpinnerComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            SpinnerComponent = (function () {
                function SpinnerComponent() {
                }
                __decorate([
                    core_1.Input(), 
                    __metadata('design:type', Boolean)
                ], SpinnerComponent.prototype, "isDataLoading", void 0);
                __decorate([
                    core_1.Input(), 
                    __metadata('design:type', Object)
                ], SpinnerComponent.prototype, "marginTop", void 0);
                __decorate([
                    core_1.Input(), 
                    __metadata('design:type', Object)
                ], SpinnerComponent.prototype, "position", void 0);
                SpinnerComponent = __decorate([
                    core_1.Component({
                        selector: "spinner",
                        templateUrl: "app/shared/template/spinner.template.html",
                        styles: ["\n                .spinner {\n                  -webkit-animation: rotator 1.4s linear infinite;\n                          animation: rotator 1.4s linear infinite;\n                }\n                \n                @-webkit-keyframes rotator {\n                  0% {\n                    -webkit-transform: rotate(0deg);\n                            transform: rotate(0deg);\n                  }\n                  100% {\n                    -webkit-transform: rotate(270deg);\n                            transform: rotate(270deg);\n                  }\n                }\n                \n                @keyframes rotator {\n                  0% {\n                    -webkit-transform: rotate(0deg);\n                            transform: rotate(0deg);\n                  }\n                  100% {\n                    -webkit-transform: rotate(270deg);\n                            transform: rotate(270deg);\n                  }\n                }\n                .path {\n                  stroke-dasharray: 187;\n                  stroke-dashoffset: 0;\n                  -webkit-transform-origin: center;\n                          transform-origin: center;\n                  -webkit-animation: dash 1.4s ease-in-out infinite, colors 5.6s ease-in-out infinite;\n                          animation: dash 1.4s ease-in-out infinite, colors 5.6s ease-in-out infinite;\n                }\n                \n                @-webkit-keyframes colors {\n                  0% {\n                    stroke: #4285F4;\n                  }\n                  25% {\n                    stroke: #DE3E35;\n                  }\n                  50% {\n                    stroke: #F7C223;\n                  }\n                  75% {\n                    stroke: #1B9A59;\n                  }\n                  100% {\n                    stroke: #4285F4;\n                  }\n                }\n                \n                @keyframes colors {\n                  0% {\n                    stroke: #4285F4;\n                  }\n                  25% {\n                    stroke: #DE3E35;\n                  }\n                  50% {\n                    stroke: #F7C223;\n                  }\n                  75% {\n                    stroke: #1B9A59;\n                  }\n                  100% {\n                    stroke: #4285F4;\n                  }\n                }\n                @-webkit-keyframes dash {\n                  0% {\n                    stroke-dashoffset: 187;\n                  }\n                  50% {\n                    stroke-dashoffset: 46.75;\n                    -webkit-transform: rotate(135deg);\n                            transform: rotate(135deg);\n                  }\n                  100% {\n                    stroke-dashoffset: 187;\n                    -webkit-transform: rotate(450deg);\n                            transform: rotate(450deg);\n                  }\n                }\n                @keyframes dash {\n                  0% {\n                    stroke-dashoffset: 187;\n                  }\n                  50% {\n                    stroke-dashoffset: 46.75;\n                    -webkit-transform: rotate(135deg);\n                            transform: rotate(135deg);\n                  }\n                  100% {\n                    stroke-dashoffset: 187;\n                    -webkit-transform: rotate(450deg);\n                            transform: rotate(450deg);\n                  }\n}\n"]
                    }), 
                    __metadata('design:paramtypes', [])
                ], SpinnerComponent);
                return SpinnerComponent;
            }());
            exports_1("SpinnerComponent", SpinnerComponent);
        }
    }
});
