import {Component} from "angular2/core";
import {UserService} from "../../login/service/user.service";
import {Router} from "angular2/router";

@Component({
    selector: "home",
    templateUrl: "app/shared/template/home.template.html",
    styles: [`
            .btn-success{
                margin-top: 15%;
                width: 15%;
                height: 10%;
            }
    `]
})
export class HomeComponent {

    constructor (private _userService:UserService, private _router:Router) {
    }

    tryLogin(){

        if ( localStorage["access_token"] ){

            this._userService.relogin(localStorage["access_token"]).subscribe(res => {
                this._router.navigate(["/Navigation/Chatter"]);
            }, error => {
                console.error(error);
                if (error.status == 401){
                    this._router.navigate(["/Navigation/Signin"]);
                }
            });
        }else{
            this._router.navigate(["/Navigation/Signin"]);
        }
    }

}