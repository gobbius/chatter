System.register(["angular2/core", "angular2/router", "../directive/active-tab.directive"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, active_tab_directive_1;
    var NavbarComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (active_tab_directive_1_1) {
                active_tab_directive_1 = active_tab_directive_1_1;
            }],
        execute: function() {
            NavbarComponent = (function () {
                function NavbarComponent(_router) {
                    this._router = _router;
                    this.isLoggedIn = false;
                }
                NavbarComponent.prototype.logout = function () {
                    localStorage.clear();
                    this.isLoggedIn = false;
                    this._router.navigate(["Default"]);
                };
                __decorate([
                    core_1.Input("is-logged-in"), 
                    __metadata('design:type', Boolean)
                ], NavbarComponent.prototype, "isLoggedIn", void 0);
                NavbarComponent = __decorate([
                    core_1.Component({
                        selector: "navbar",
                        templateUrl: "app/shared/template/navbar.template.html",
                        styles: ["\n                .tab-active {\n                    background: #0a68b4 !important;\n                    color: #fff !important;\n                }\n            "],
                        directives: [router_1.ROUTER_DIRECTIVES, active_tab_directive_1.ActiveTabDirective]
                    }), 
                    __metadata('design:paramtypes', [router_1.Router])
                ], NavbarComponent);
                return NavbarComponent;
            }());
            exports_1("NavbarComponent", NavbarComponent);
        }
    }
});
