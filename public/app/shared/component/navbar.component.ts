import {Component, OnInit, Input} from "angular2/core";
import {ROUTER_DIRECTIVES, Router} from "angular2/router";
import {ActiveTabDirective} from "../directive/active-tab.directive";

@Component({
    selector: "navbar",
    templateUrl: "app/shared/template/navbar.template.html",
    styles: [`
                .tab-active {
                    background: #0a68b4 !important;
                    color: #fff !important;
                }
            `],
    directives: [ROUTER_DIRECTIVES, ActiveTabDirective]
})

export class NavbarComponent{

    @Input("is-logged-in") isLoggedIn:boolean = false;

    constructor(private _router:Router) {
        
    }

    logout() {
        localStorage.clear();
        this.isLoggedIn = false;
        this._router.navigate(["Default"]);
    }

}