import {bootstrap} from 'angular2/platform/browser';
import {Main} from './main';
import {HTTP_PROVIDERS} from "angular2/http";
import {ROUTER_PROVIDERS} from "angular2/router";
import {UserService} from "./login/service/user.service";


bootstrap(Main, [HTTP_PROVIDERS, ROUTER_PROVIDERS, UserService])
    .catch(error => {
        console.error(error);
    });