import {Http, Headers} from "angular2/http";
import {Injectable} from "angular2/core";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import {Observable} from "rxjs/Observable";
import {CustomConfig} from "../../shared/resources/CustomConfig";

@Injectable()
export class UserService {

    private _serviceUrl:string = CustomConfig.SERVICE_ENDPOINT;
    private _isLoggedIn:boolean = false;

    constructor(private _http:Http) {

    }

    get isLoggedIn():boolean {
        if (localStorage["access_token"]) {
            return true;
        } else {
            return false;
        }
    }

    set isLoggedIn(value:boolean) {
        this._isLoggedIn = value;
    }

    login(userdata:{[key:string]:string}):Observable<any> {

        let loginUrl = this._serviceUrl + CustomConfig.API_PREFIX_MAP["login"] + "?username=";
        let headers = new Headers({
            "Content-Type": "application/x-www-form-urlencoded"
        });
        loginUrl += userdata["username"] + "&password=" + userdata["password"];
        return this._http.get(loginUrl, {
            headers: headers
        }).map(res => res.json());
    }

    relogin(token) {
        let authUrl = this._serviceUrl + CustomConfig.API_PREFIX_MAP["authorization"];
        let headers = new Headers({
            "Authorization": token
        });
        return this._http.get(authUrl, {
            headers: headers
        }).map(res => res.status);
    }

    signup(userdata:{[key:string]:string}):Observable<any> {
        let signupUrl = this._serviceUrl + CustomConfig.API_PREFIX_MAP["signup"];
        let headers = new Headers({
            "Content-Type": "application/json"
        });
        return this._http.post(signupUrl, JSON.stringify(userdata), {
            headers: headers
        }).map(res => res.status);
    }
}