System.register(["angular2/http", "angular2/core", "rxjs/add/operator/map", "rxjs/add/operator/catch", "../../shared/resources/CustomConfig"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var http_1, core_1, CustomConfig_1;
    var UserService;
    return {
        setters:[
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (_1) {},
            function (_2) {},
            function (CustomConfig_1_1) {
                CustomConfig_1 = CustomConfig_1_1;
            }],
        execute: function() {
            UserService = (function () {
                function UserService(_http) {
                    this._http = _http;
                    this._serviceUrl = CustomConfig_1.CustomConfig.SERVICE_ENDPOINT;
                    this._isLoggedIn = false;
                }
                Object.defineProperty(UserService.prototype, "isLoggedIn", {
                    get: function () {
                        if (localStorage["access_token"]) {
                            return true;
                        }
                        else {
                            return false;
                        }
                    },
                    set: function (value) {
                        this._isLoggedIn = value;
                    },
                    enumerable: true,
                    configurable: true
                });
                UserService.prototype.login = function (userdata) {
                    var loginUrl = this._serviceUrl + CustomConfig_1.CustomConfig.API_PREFIX_MAP["login"] + "?username=";
                    var headers = new http_1.Headers({
                        "Content-Type": "application/x-www-form-urlencoded"
                    });
                    loginUrl += userdata["username"] + "&password=" + userdata["password"];
                    return this._http.get(loginUrl, {
                        headers: headers
                    }).map(function (res) { return res.json(); });
                };
                UserService.prototype.relogin = function (token) {
                    var authUrl = this._serviceUrl + CustomConfig_1.CustomConfig.API_PREFIX_MAP["authorization"];
                    var headers = new http_1.Headers({
                        "Authorization": token
                    });
                    return this._http.get(authUrl, {
                        headers: headers
                    }).map(function (res) { return res.status; });
                };
                UserService.prototype.signup = function (userdata) {
                    var signupUrl = this._serviceUrl + CustomConfig_1.CustomConfig.API_PREFIX_MAP["signup"];
                    var headers = new http_1.Headers({
                        "Content-Type": "application/json"
                    });
                    return this._http.post(signupUrl, JSON.stringify(userdata), {
                        headers: headers
                    }).map(function (res) { return res.status; });
                };
                UserService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [http_1.Http])
                ], UserService);
                return UserService;
            }());
            exports_1("UserService", UserService);
        }
    }
});
