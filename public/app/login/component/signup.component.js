System.register(["angular2/core", "../service/user.service", "angular2/router", "angular2/common", "../../shared/resources/password.validator", "../../shared/component/navbar.component"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, user_service_1, router_1, common_1, password_validator_1, navbar_component_1;
    var SignUpComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (user_service_1_1) {
                user_service_1 = user_service_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (common_1_1) {
                common_1 = common_1_1;
            },
            function (password_validator_1_1) {
                password_validator_1 = password_validator_1_1;
            },
            function (navbar_component_1_1) {
                navbar_component_1 = navbar_component_1_1;
            }],
        execute: function() {
            SignUpComponent = (function () {
                function SignUpComponent(_userService, _router, formBuilder) {
                    this._userService = _userService;
                    this._router = _router;
                    this._isUserExist = false;
                    this._isConfirmed = false;
                    this._form = formBuilder.group({
                        username: [null, common_1.Validators.required],
                        password: [null, common_1.Validators.compose([common_1.Validators.required, common_1.Validators.minLength(6)])],
                        confirm: [null, common_1.Validators.required]
                    }, { validator: password_validator_1.PasswordValidator.confirmPassword });
                }
                Object.defineProperty(SignUpComponent.prototype, "isUserExist", {
                    get: function () {
                        return this._isUserExist;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(SignUpComponent.prototype, "isConfirmed", {
                    get: function () {
                        return this._isConfirmed;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(SignUpComponent.prototype, "form", {
                    get: function () {
                        return this._form;
                    },
                    enumerable: true,
                    configurable: true
                });
                SignUpComponent.prototype.ngOnInit = function () {
                    $("#signUpBtn").click(function () {
                        $(this).toggleClass("active");
                    });
                };
                SignUpComponent.prototype.signUp = function () {
                    var _this = this;
                    $(this).prop("disabled", true);
                    this._userService.signup({
                        username: this._form.find("username").value,
                        password: this._form.find("password").value
                    }).subscribe(function (result) {
                        _this._isConfirmed = true;
                    }, function (error) {
                        if (error._body == "Validation error") {
                            _this._isUserExist = true;
                            _.delay(function () { return _this._isUserExist = false; }, 2000);
                        }
                        $("#signUpBtn").toggleClass("active");
                        $("#signUpBtn").prop("disabled", false);
                    }, function () {
                        $("#signUpBtn").toggleClass("active");
                        $("#signUpBtn").prop("disabled", false);
                        _this._isUserExist = false;
                        _.delay(function () { return _this._router.navigate(["Signin"]); }, 2000);
                    });
                };
                SignUpComponent = __decorate([
                    core_1.Component({
                        selector: "signup-form",
                        templateUrl: "app/login/template/signup.template.html",
                        styles: ["\n  \n        .ng-touched.ng-invalid{\n            border-color: red;\n        }\n        .login-form {\n            background-color: #f9f2f4;\n            border-radius: 2%;\n            color: black;\n        }\n        .login-form legend {\n            color: black;\n        }\n        .login-form button {\n            width: 50%;\n        }\n        .spinner {\n            display: inline-block;\n            opacity: 0;\n            width: 0;\n        \n            -webkit-transition: opacity 0.25s, width 0.25s;\n            -moz-transition: opacity 0.25s, width 0.25s;\n            -o-transition: opacity 0.25s, width 0.25s;\n            transition: opacity 0.25s, width 0.25s;\n        }\n        .has-spinner.active {\n            cursor:progress;\n        }\n        \n        .has-spinner.active .spinner {\n            opacity: 1;\n            width: auto; /* This doesn't work, just fix for unkown width elements */\n        }\n        \n        .has-spinner.btn-mini.active .spinner {\n            width: 10px;\n        }\n        \n        .has-spinner.btn-small.active .spinner {\n            width: 13px;\n        }\n        \n        .has-spinner.btn.active .spinner {\n            width: 16px;\n        }\n        \n        .has-spinner.btn-large.active .spinner {\n            width: 19px;\n}      \n    "],
                        directives: [navbar_component_1.NavbarComponent]
                    }), 
                    __metadata('design:paramtypes', [user_service_1.UserService, router_1.Router, common_1.FormBuilder])
                ], SignUpComponent);
                return SignUpComponent;
            }());
            exports_1("SignUpComponent", SignUpComponent);
        }
    }
});
