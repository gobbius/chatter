System.register(["angular2/core", "../service/user.service", "angular2/router", "../../shared/component/navbar.component"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, user_service_1, router_1, navbar_component_1;
    var SigninComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (user_service_1_1) {
                user_service_1 = user_service_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (navbar_component_1_1) {
                navbar_component_1 = navbar_component_1_1;
            }],
        execute: function() {
            SigninComponent = (function () {
                function SigninComponent(_userService, _router) {
                    this._userService = _userService;
                    this._router = _router;
                    this._isErrorInCreds = false;
                }
                Object.defineProperty(SigninComponent.prototype, "isErrorInCreds", {
                    get: function () {
                        return this._isErrorInCreds;
                    },
                    enumerable: true,
                    configurable: true
                });
                SigninComponent.prototype.ngOnInit = function () {
                    $("#loginBtn").click(function () {
                        $(this).toggleClass("active");
                    });
                };
                SigninComponent.prototype.submitForm = function (form) {
                    var _this = this;
                    $("#loginBtn").prop("disabled", true);
                    this._userService.login(form)
                        .subscribe(function (res) {
                        _this._isErrorInCreds = false;
                        _this._userService.isLoggedIn = true;
                        localStorage["access_token"] = res.token;
                        $("#loginBtn").toggleClass("active");
                        $("#loginBtn").prop("disabled", false);
                        _this._router.navigate(["/Navigation/Chatter"]);
                    }, function (err) {
                        _this._isErrorInCreds = true;
                        $("#loginBtn").toggleClass("active");
                        $("#loginBtn").prop("disabled", false);
                    });
                };
                SigninComponent = __decorate([
                    core_1.Component({
                        selector: "signin-form",
                        templateUrl: "app/login/template/signin.template.html",
                        styles: ["\n  \n        .ng-touched.ng-invalid{\n            border-color: red;\n        }\n        .login-form {\n            background-color: #f9f2f4;\n            border-radius: 2%;\n            color: black;\n        }\n        .login-form legend {\n            color: black;\n        }\n        .login-form button {\n            width: 50%;\n        }\n        .spinner {\n            display: inline-block;\n            opacity: 0;\n            width: 0;\n        \n            -webkit-transition: opacity 0.25s, width 0.25s;\n            -moz-transition: opacity 0.25s, width 0.25s;\n            -o-transition: opacity 0.25s, width 0.25s;\n            transition: opacity 0.25s, width 0.25s;\n        }\n        .has-spinner.active {\n            cursor:progress;\n        }\n        \n        .has-spinner.active .spinner {\n            opacity: 1;\n            width: auto; /* This doesn't work, just fix for unkown width elements */\n        }\n        \n        .has-spinner.btn-mini.active .spinner {\n            width: 10px;\n        }\n        \n        .has-spinner.btn-small.active .spinner {\n            width: 13px;\n        }\n        \n        .has-spinner.btn.active .spinner {\n            width: 16px;\n        }\n        \n        .has-spinner.btn-large.active .spinner {\n            width: 19px;\n}      \n    "],
                        directives: [router_1.ROUTER_DIRECTIVES, navbar_component_1.NavbarComponent]
                    }), 
                    __metadata('design:paramtypes', [user_service_1.UserService, router_1.Router])
                ], SigninComponent);
                return SigninComponent;
            }());
            exports_1("SigninComponent", SigninComponent);
        }
    }
});
