import {Component, OnInit} from "angular2/core";
import {UserService} from "../service/user.service";
import {ROUTER_DIRECTIVES, Router} from "angular2/router";
import {NavbarComponent} from "../../shared/component/navbar.component";


@Component({
    selector: "signin-form",
    templateUrl: "app/login/template/signin.template.html",
    styles: [`
  
        .ng-touched.ng-invalid{
            border-color: red;
        }
        .login-form {
            background-color: #f9f2f4;
            border-radius: 2%;
            color: black;
        }
        .login-form legend {
            color: black;
        }
        .login-form button {
            width: 50%;
        }
        .spinner {
            display: inline-block;
            opacity: 0;
            width: 0;
        
            -webkit-transition: opacity 0.25s, width 0.25s;
            -moz-transition: opacity 0.25s, width 0.25s;
            -o-transition: opacity 0.25s, width 0.25s;
            transition: opacity 0.25s, width 0.25s;
        }
        .has-spinner.active {
            cursor:progress;
        }
        
        .has-spinner.active .spinner {
            opacity: 1;
            width: auto; /* This doesn't work, just fix for unkown width elements */
        }
        
        .has-spinner.btn-mini.active .spinner {
            width: 10px;
        }
        
        .has-spinner.btn-small.active .spinner {
            width: 13px;
        }
        
        .has-spinner.btn.active .spinner {
            width: 16px;
        }
        
        .has-spinner.btn-large.active .spinner {
            width: 19px;
}      
    `],
    directives: [ROUTER_DIRECTIVES, NavbarComponent]
})

export class SigninComponent implements OnInit {
    
    private _isErrorInCreds:boolean = false;

    constructor(private _userService:UserService, private _router:Router) {
    }

    get isErrorInCreds():boolean {
        return this._isErrorInCreds;
    }

    ngOnInit() {
        $("#loginBtn").click(function () {
            $(this).toggleClass("active");

        });
    }

    submitForm(form) {
        $("#loginBtn").prop("disabled", true);
        this._userService.login(form)
            .subscribe(res => {
                this._isErrorInCreds = false;
                this._userService.isLoggedIn = true;
                localStorage["access_token"] = res.token;
                $("#loginBtn").toggleClass("active");
                $("#loginBtn").prop("disabled", false);
                this._router.navigate(["/Navigation/Chatter"]);
            }, err => {
                this._isErrorInCreds = true;
                $("#loginBtn").toggleClass("active");
                $("#loginBtn").prop("disabled", false);
            });
    }

}