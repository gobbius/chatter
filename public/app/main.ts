import {Component, OnInit} from "angular2/core";
import {UserService} from "./login/service/user.service";
import {HomeRouter} from "./shared/router/home.router";
import {RouteConfig, Router} from "angular2/router";
import {HomeComponent} from "./shared/component/home.component";
import {NavbarComponent} from "./shared/component/navbar.component";

@RouteConfig([
    {
        path: "/...",
        name: "Navigation",
        component: HomeRouter
    },
    {
        path: "/",
        name: "Default",
        component: HomeComponent,
        useAsDefault: true
    }
])

@Component({
    selector: "main",
    template: `<div class="container-fluid">
                        <navbar [is-logged-in]="isLoggedIn"></navbar>
                        <div style="padding-bottom: 10%;"></div>
                        <homerouter></homerouter>
                  </div>`,
    directives: [HomeRouter, NavbarComponent]
})
export class Main {

    constructor(private _userService:UserService, private _router:Router) {
        if ( !this.isLoggedIn ){
            this._router.navigate(["Default"]);
        }else{
            this._router.navigate(["/Navigation/Chatter"]);
        }
    }

    get isLoggedIn():boolean {
        return this._userService.isLoggedIn;;
    }


}