System.register(['angular2/platform/browser', './main', "angular2/http", "angular2/router", "./login/service/user.service"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var browser_1, main_1, http_1, router_1, user_service_1;
    return {
        setters:[
            function (browser_1_1) {
                browser_1 = browser_1_1;
            },
            function (main_1_1) {
                main_1 = main_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (user_service_1_1) {
                user_service_1 = user_service_1_1;
            }],
        execute: function() {
            browser_1.bootstrap(main_1.Main, [http_1.HTTP_PROVIDERS, router_1.ROUTER_PROVIDERS, user_service_1.UserService])
                .catch(function (error) {
                console.error(error);
            });
        }
    }
});
