var PORT = process.env.PORT || 3000;
var express = require("express");
var path = require("path");
var bodyParser = require("body-parser");
var cookieParser = require("cookie-parser");
var app = express();
var http = require("http").Server(app);
var io = require("socket.io")(http);
app.connection = require("./server/connection");

app.use("/scripts", express.static(__dirname + "/node_modules"));
app.use("/assets", express.static(__dirname + "/assets"));
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app = require("./server/requestmapping")(app, path.join(__dirname, "/public/index.html"));

app.connection.sequelize.sync()
.then(() => {
    http.listen(PORT, () => {
        console.log("Express listening on port: " + PORT + ".");
    });
});