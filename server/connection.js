var Sequelize = require('sequelize');
var env = process.env.NODE_ENV || "development";
var sequelize;

if (env === "production") {
    sequelize = new Sequelize(process.env.DATABASE_URL, {
        dialect: "postgres"
    });
} else {
    sequelize = new Sequelize(undefined, undefined, undefined, {
        'dialect': 'sqlite',
        'storage': __dirname + '/data/development.sqlite'
    });
}

var connection = {};

connection.user = sequelize.import(__dirname + "/models/user.js");
connection.token = sequelize.import(__dirname + "/models/token.js");

connection.sequelize = sequelize;
connection.Sequelize = Sequelize;

module.exports = connection;