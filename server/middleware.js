var crypto = require("crypto-js");

module.exports = (connection) => {
    return {
        requireAuthentication: (req, res, next) => {

            var token = req.get("Authorization") || "";
            connection.token.findOne({
                where: {
                    tokenHash: crypto.MD5(token).toString()
                }
            }).then(tokenInstance => {
                if (!tokenInstance) {
                    throw new Error();
                }
                req.token = tokenInstance;
                return connection.user.findByToken(token);
            })
                .then(user => {
                    req.user = user;
                    next();
                })
                .catch(exc => res.status(401).send());
        }
    };
};