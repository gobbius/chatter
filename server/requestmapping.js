var _ = require("underscore");
var crypto = require("crypto-js");

module.exports = (server, indexPath) => {
    var middleware = require("./middleware")(server.connection);

    server.get("/api/login", (req, res) => {
        console.log(req.query);
        server.connection.user.authenticate(
            {
                username: req.query.username,
                password: req.query.password
            }
        ).then(user => {
            var token = user.generateToken("authentication");
            server.connection.token.create({
                token: token
            }).then(token => {
                res.json({token: token.token});
            });
        }, err => res.status(401).send())
            .catch(exc => {
                res.status(500).send();
            });

    });

    server.get("/api/authorization", (req, res) => {
        var token = req.get("Authorization") || "";
        server.connection.token.findOne({
            where: {
                tokenHash: crypto.MD5(token).toString()
            }
        }).then(tokenInstance => {
            if (!tokenInstance) {
                res.status(401).send();
            }
            return server.connection.user.findByToken(token);
        }).then(user => {
                res.status(200).send();
        }).catch(exc => res.status(500).send());
    });
    
    server.post("/api/signup", (req, res) => {
        server.connection.user.create(_.pick(req.body, "username", "password"))
        .then(user => {
            res.json(user);
        }, err => {
            res.status(500).send(err.message);
        }).catch(ex => {
            res.status(404).send();
        });
    });

    server.all("*", (req, res) => {
        res.status(200).sendFile(indexPath);
    });
    return server;
};
