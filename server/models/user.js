var bcrypt = require("bcrypt");
var _ = require("underscore");
var crypto = require("crypto-js");
var jwt = require("jsonwebtoken");

module.exports = (sequelize, DataTypes) => {
    var user = sequelize.define('user', {
            username: {
                type: DataTypes.STRING,
                allowNull: false,
                unique: true
            },
            salt: {
                type: DataTypes.STRING
            },
            password_hash: {
                type: DataTypes.STRING
            },
            password: {
                type: DataTypes.VIRTUAL,
                allowNull: false,
                validate: {
                    len: [4, 100]
                },
                set: function (value) {
                    var salt = bcrypt.genSaltSync(10);
                    var hashedPassword = bcrypt.hashSync(value, salt);
                    this.setDataValue("password", value);
                    this.setDataValue("salt", salt);
                    this.setDataValue("password_hash", hashedPassword);
                }
            }
        },
        {
            hooks: {
                beforeValidate: (user, options) => {
                    if (typeof user.username === "string") {
                        user.username = user.username.toLocaleLowerCase();
                    }
                }
            },
            classMethods: {
                authenticate: function (body) {
                    return new Promise((resolve, reject) => {
                        var where = {};
                        where.username = body.username;
                        user.findOne({
                                where: where
                            })
                            .then(user => {
                                if (!user || !bcrypt.compareSync(body.password, user.get("password_hash"))) {
                                    return reject("rejected");
                                }
                                resolve(user);
                            }, err => {
                                reject();
                            })
                            .catch(exc => {
                                reject();
                            });
                    });
                },
                findByToken: function (token) {
                    return new Promise((resolve, reject) => {
                        try {
                            var decodedJWT = jwt.verify(token, "test666");
                            // console.log(token);
                            // console.log(decodedJWT);
                            var bytes = crypto.AES.decrypt(decodedJWT.token, "test123!@#$");
                            var tokenData = JSON.parse(bytes.toString(crypto.enc.Utf8));

                            user.findById(tokenData.id)
                                .then(user => {
                                    if (user) {
                                        resolve(user);
                                    } else {
                                        reject();
                                    }
                                }, () => {
                                    reject();
                                });
                        } catch (exc) {
                            reject();
                        }
                    });
                }
            },
            instanceMethods: {
                toPublicJSON: function () {
                    return _.pick(this.toJSON(), "id", "email");
                },
                generateToken: function (type) {

                    if (!_.isString(type)) {
                        return undefined;
                    }
                    try {
                        var stringData = JSON.stringify({id: this.get("id"), type: type});
                        var encryptedData = crypto.AES.encrypt(stringData, "test123!@#$").toString();
                        var token = jwt.sign({
                            token: encryptedData
                        }, "test666");
                        return token;
                    } catch (exc) {
                        return undefined;
                    }
                }
            }
        });
    return user;
};